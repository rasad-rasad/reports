# 1)mysql ucun data disk yaradir
# 2)mysql config file yaradir
# 3)merkezi sebeke yaradir mysql ile petclinic-app arasinda
# 4)mysql docker hub-dan pull edir ve run edir


docker volume create mysql_data

docker volume create mysql_config

docker network create mysqlnet

docker run -d -p 3306:3306 -e MYSQL_ROOT_PASSWORD=root rasad/petclinic-db

docker run -it --rm -d -v mysql_data:/var/lib/mysql \
    -v mysql_config:/etc/mysql/conf.d \
    --network mysqlnet \
    --name mysqlserver \ -e MYSQL_PASSWORD=petclinic \
    -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=petclinic
    

